import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const PageBannerContact = ({ title, name }) => {
  return (
    <section className="bgHome position-home-relative">
      <div className="col-lg-5  text-about-content position-home">
        <p>
          Hoje, somos um grupo. Um grupo com base nos{" "}
          <span>princípios e valores cristãos.</span> Acreditamos que podemos
          mudar o mundo, com foco nas pessoas e no seu crescimento.
        </p>
      </div>
    </section>
  );
};

export default PageBannerContact;
