const nodemailer = require("nodemailer");
const config = require("../../config.local");

export default function sendEmail(req, res) {
  var transporter = nodemailer.createTransport({
    host: "smtp.mailtrap.io",
    port: 2525,
    secure: false,
    auth: {
      user: config.USERMAIL,
      pass: config.PASSMAIL,
    },
  });
  var mailOptions = {
    from: `${req.body.name} <${req.body.email}>`,
    to: "contato@centralgospel.com",
    subject: "Contato do Site - Central Gospel",
    text: "Contato do Site - Central Gospel",
    html: `
    <span>Nome: <b>${req.body.name}</b></span>
    <br/>
    <span>Telefone: <b>${req.body.phone}</b></span>
    <br/>
    <span>E-mail: <b>${req.body.email}</b></span>
    <br/>
    <span>Mensagem: <b>${req.body.message}</b></span>`,
  };
  transporter.sendMail(mailOptions, function (err, info) {
    if (err) console.log(err);
    else console.log(info);
  });

  console.log(req.body);
  res.send("success");
}
