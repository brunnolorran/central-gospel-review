import React from "react";
import Footer from "@/components/footer3";
import Layout from "@/components/layout";
import PageBannerAbout from "@/components/page-banner-about";
import HeaderOne from "@/components/header-one";
import SearchContextProvider from "@/context/search-context";
import ParallaxOne from "@/components/parallax-1";
import MenuContextProvider from "@/context/menu-context";
import History from "@/components/history";

const AboutPage = () => {
  return (
    <MenuContextProvider>
      <SearchContextProvider>
        <Layout PageTitle="Sobre Nós">
          <HeaderOne />
          <PageBannerAbout />
          <ParallaxOne />
          <History />
          <Footer />
        </Layout>
      </SearchContextProvider>
    </MenuContextProvider>
  );
};

export default AboutPage;
